module gitlab.com/zenpagos/tools

go 1.14

require (
	github.com/smartystreets/goconvey v1.6.4
	github.com/speps/go-hashids v2.0.0+incompatible
)
